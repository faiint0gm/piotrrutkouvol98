﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour {

    public GameObject target;
    public Vector3 offset;
    public float smooth = 0.25f;
    private void Update()
    {
        if (target != null)
            transform.position = Vector3.Lerp(transform.position, target.transform.position + offset, smooth);
    }
}
