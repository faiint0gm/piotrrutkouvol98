﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject ballPrefab;
    public Vector3 spawnPoint;
    public Camera mainCamera;
    public float maxLineDistance = 2f;
    public Transform catapultBack;
    public Transform catapultFront;
    public Camera inputCamera;
    FollowCam cameraFollow;
    List<GameObject> balls;
    int actualBallId = 0;
    float timeToRespawn = 1f;

    public static GameManager instance;
    private void Awake()
    {
        instance = this;
        cameraFollow = mainCamera.GetComponent<FollowCam>();
        actualBallId = -1;
        balls = new List<GameObject>();
    }

    private void Start()
    {
        Spawn();
    }

    void Spawn()
    {
        actualBallId++;
        GameObject go = Instantiate(ballPrefab, spawnPoint, Quaternion.Euler(0, 0, 0));
        balls.Add(go);
        cameraFollow.target = balls[actualBallId];
    }

    void SetAndSpawn()
    {

        mainCamera.GetComponent<FollowCam>().enabled = false;
        balls[actualBallId].GetComponent<BallDrag>().enabled = false;
        Spawn();
    }

    public IEnumerator StartRespawn()
    {
        yield return new WaitForSeconds(timeToRespawn);
            SetAndSpawn();
        yield break;
    }
}
