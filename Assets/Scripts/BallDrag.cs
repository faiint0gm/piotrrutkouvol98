﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDrag : MonoBehaviour {

    Camera InputCamera;
    FollowCam followCam;
    float maxDistance;
    LineRenderer frontLine;
    LineRenderer backLine;
    bool isClicked;
    SpringJoint2D springJoint;
    Transform catapultBack;
    Transform catapultFront;
    Ray rayToMouse;
    Ray catapultToBall;
    float maxDistanceSquare;
    float ballRadius;
    Vector2 previousVelocity;
    Rigidbody2D rb2D;

    private void Awake()
    {
        catapultBack = GameManager.instance.catapultBack;
        catapultFront = GameManager.instance.catapultFront;
        frontLine = catapultFront.GetComponent<LineRenderer>();
        backLine = catapultBack.GetComponent<LineRenderer>();
        maxDistance = GameManager.instance.maxLineDistance;
        InputCamera = GameManager.instance.inputCamera;
        followCam = GameManager.instance.mainCamera.GetComponent<FollowCam>();
        followCam.enabled = true;
        springJoint = GetComponent<SpringJoint2D>();
        rb2D = GetComponent<Rigidbody2D>();
        springJoint.connectedBody = catapultBack.GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        SetupLines();
        
        rayToMouse = new Ray(catapultBack.position, Vector3.zero);
        catapultToBall = new Ray(frontLine.transform.position, Vector3.zero);
        maxDistanceSquare = Mathf.Pow(maxDistance, 2);
        CircleCollider2D circle = GetComponent<CircleCollider2D>();
        ballRadius = circle.radius;
    }

    private void Update()
    {
        if (isClicked)
            Drag();

        if (springJoint != null)
        {
            if (!rb2D.isKinematic && previousVelocity.sqrMagnitude > rb2D.velocity.sqrMagnitude)
            {
                Destroy(springJoint);
                //followCam.enabled = true;
                rb2D.velocity = previousVelocity;

            }

            if (!isClicked)
            {
                previousVelocity = rb2D.velocity;
            }
            UpdateLines();
        }
        else
        {
            frontLine.enabled = false;
            backLine.enabled = false;
        }

        if (transform.position.y <= -3.6f)
        {
            StartCoroutine(GameManager.instance.StartRespawn());
            Launched();
        }
    }

    void Launched()
    {
        enabled = false;
    }

    void SetupLines()
    {
        frontLine.enabled = true;
        backLine.enabled = true;
        frontLine.SetPosition(0, frontLine.transform.position);
        backLine.SetPosition(0, backLine.transform.position);
        backLine.sortingOrder = 1;
        frontLine.sortingOrder = 3;
    }

    void UpdateLines()
    {
        Vector2 catapultToBallVect = transform.position - frontLine.transform.position;
        catapultToBall.direction = catapultToBallVect;
        Vector3 clickPoint = catapultToBall.GetPoint(catapultToBallVect.magnitude + ballRadius);
        frontLine.SetPosition(1, clickPoint);
        backLine.SetPosition(1, clickPoint);
    }

    private void OnMouseDown()
    {
        
        springJoint.enabled = false;
        isClicked = true;
    }

    private void OnMouseUp()
    {
        followCam.enabled = true;
        springJoint.enabled = true;
        rb2D.isKinematic = false;
        isClicked = false;
    }

    private void Drag()
    {
        followCam.enabled = false;
        Vector3 mousePoint = InputCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 catapultToMouse = mousePoint - catapultBack.position;

        if(catapultToMouse.sqrMagnitude>maxDistanceSquare)
        {
            rayToMouse.direction = catapultToMouse;
            mousePoint = rayToMouse.GetPoint(maxDistance);
        }

        mousePoint.z = 0f;
        transform.position = mousePoint;
    }


}
