﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour {

    public Rigidbody rb;

    public static List<Gravity> GravityObjects;

    private void FixedUpdate()
    {
        foreach (Gravity gravityObject in GravityObjects)
        {
            if (gravityObject != this)
                UseGravity(gravityObject);
        }
    }

    private void OnEnable()
    {
        if (GravityObjects == null)
            GravityObjects = new List<Gravity>();
        GravityObjects.Add(this);
    }

    private void OnDisable()
    {
        GravityObjects.Remove(this);
    }

    void UseGravity(Gravity otherObject)
    {
        Rigidbody otherRb = otherObject.rb;

        Vector3 dir = rb.position - otherRb.position;
        float dist = dir.magnitude;

        if (dist == 0f)
            Merge(otherObject);

        float gravityForce = 700 * (rb.mass * otherRb.mass) / Mathf.Pow(dist, 2);  // G * (m1*m2)/r^2
        Vector3 force = dir.normalized * gravityForce;

        otherRb.AddForce(force);
    }

    void Merge (Gravity objectToMerge)
    {
        objectToMerge.enabled = false;
        float newMass = rb.mass + objectToMerge.rb.mass;
        Vector3 newSize = transform.localScale + objectToMerge.transform.localScale;

        GameManagerGrav.instance.MergeBalls(this,objectToMerge,newMass,newSize);
    }
}
