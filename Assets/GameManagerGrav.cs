﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameManagerGrav : MonoBehaviour {

    public Camera mainCamera;
    public TextMeshProUGUI ballsText;
    int ballsAmount;
    public GameObject spherePrefab;
    public float minStartMass;
    public float maxStartMass;
    public Vector3 startSize;

    public static GameManagerGrav instance;
    
    private void Update()
    {
        UpdateBalls();
    }

    private void Start()
    {
        StartCoroutine(SetNewBall());
    }

    void UpdateBalls()
    {
        ballsText.text = string.Format("Balls: {0}", ballsAmount);
    }

    void NewBall(float mass, Vector3 size)
    {
        ballsAmount++;

        GameObject go = Instantiate(spherePrefab,
            new Vector3(Random.Range(mainCamera.transform.position.x - Screen.width/2, mainCamera.transform.position.x + Screen.width / 2)
            , Random.Range(mainCamera.transform.position.y - Screen.height / 2, mainCamera.transform.position.y + Screen.height / 2)
            , Random.Range(mainCamera.transform.position.z - 50 , mainCamera.transform.position.z - 200)),
            Quaternion.identity);
        go.GetComponent<Rigidbody>().mass = mass;
        go.transform.localScale = size;
    }

    public void MergeBalls(Gravity ball1, Gravity ball2, float newMass, Vector3 newSize)
    {
        ballsAmount -= 2;
        Destroy(ball1.gameObject);
        Destroy(ball2.gameObject);
        NewBall(newMass, newSize);

    }

    IEnumerator SetNewBall()
    {
        while (ballsAmount <= 250)
        {
            yield return new WaitForSeconds(0.25f);
            float startMass = Random.Range(minStartMass, maxStartMass);
            NewBall(startMass, startSize *startMass );
            yield return null;
        }
    }
}
